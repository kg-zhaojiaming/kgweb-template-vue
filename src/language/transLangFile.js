var fs = require('fs')
var xlsx = require('node-xlsx')
var path = require('path')

var data = xlsx.parse('./CMSweb界面多语言.xlsx')
// console.log(data[2]);
let table = data[4].data
// 遍历lang key
for (let j = 2; j < table[0].length; j++) {
  console.log('lang:' + table[0][j])
  let code = table[0][j]
  let obj = ''
  // 遍历每一行内容
  for (let i = 2; i < table.length; i++) {
    // 判断key是否为空
    if (table[i][0] && table[i][0] !== '') {
      // if (table[i][0] === 'article_growth') {
      //   console.log('-----> ' + table[i][j].replace(/["]+/gm, '').replace(/\r|\n/g,'<br/>'))
      // }
      if (table[i][j]) {
        obj += ',"' + table[i][0].replace('"', '') + '":"' + table[i][j].replace(/["]+/gm, '').replace(/\r|\n/g, '<br/>') + '"'
      }
    }
  }
  // for(let i = 1; i < table.length; i++) {
  //   obj[table[i][0]] = table[i][j]
  // }
  let result = '{' + obj.substr(1) + '}'
  fs.writeFile(path.resolve('../assets/lang/' + code + '.json'), result, function (err) {
    if (err) {
      console.log('Error!' + err)
      return
    }
    console.log(code + '.js写入完成')
  })
}
