// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueI18n from 'vue-i18n'
import langConfig from '@/utils/langConfig'

// css
import '@/assets/style/style.scss'
import '@/utils/flexible.js'
// utils
import loading from '@/utils/loading'
import toast from '@/utils/toast'
import errCode from '@/utils/errCode'
import setLog from '@/utils/log'
import tips from '@/utils/tips'
import FastClick from 'fastclick'

FastClick.attach(document.body)

// utils
Vue.use(loading)
Vue.use(toast)
Vue.use(errCode)
Vue.use(setLog)
Vue.use(tips)

Vue.config.productionTip = false
// i18n
Vue.use(VueI18n)

Vue.prototype.$window = window

const i18n = new VueI18n({
  locale: 'en', // 语言标识
  fallbackLocale: 'en',
  messages: langConfig.config
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  components: { App },
  template: '<App/>'
})
