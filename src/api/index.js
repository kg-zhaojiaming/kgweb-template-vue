import request from '@/utils/request'

// 埋点
export function setLog (params) {
  return request({
    method: 'post',
    url: 'gog2rdapi/cms/logger',
    data: params
  })
}
