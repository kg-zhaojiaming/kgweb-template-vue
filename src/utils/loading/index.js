import LoadingComponent from './loading'

let $vm

export default {
  install (Vue, options) {
    if (!$vm) {
      const LoadingPlugin = Vue.extend(LoadingComponent)
      $vm = new LoadingPlugin({
        el: document.createElement('div')
      })
      document.body.appendChild($vm.$el)
    }
    $vm.show = false
    let loading = {
      open () {
        $vm.show = true
      },
      close () {
        $vm.show = false
      }
    }
    if (!Vue.$loading) {
      Vue.$loading = loading
    }
    // Vue.prototype.$loading = Vue.$loading;
    Vue.mixin({
      created () {
        this.$loading = Vue.$loading
      }
    })
  }
}
