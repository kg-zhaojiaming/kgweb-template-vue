import TipsComponent from './tips'

let $vm

export default {
  install (Vue, options) {
    if (!$vm) {
      const LoadingPlugin = Vue.extend(TipsComponent)
      $vm = new LoadingPlugin({
        el: document.createElement('div')
      })
      document.body.appendChild($vm.$el)
    }
    $vm.isShowTip = false
    let tips = {
      open (x, y, obj) {
        $vm.isShowTip = true
        $vm.tipObj = obj
        $vm.tipLeft = x
        $vm.tipTop = y
      },
      close () {
        $vm.show = false
      }
    }
    if (!Vue.$tips) {
      Vue.$tips = tips
    }
    // Vue.prototype.$loading = Vue.$loading;
    Vue.mixin({
      created () {
        this.$tips = Vue.$tips
      }
    })
  }
}
