let timer = null
export default{
  install (Vue, options) {
    Vue.prototype.showToast = function (content, Vue, options) {
      clearTimeout(timer)
      document.getElementById('toastWrap').innerHTML = '<div class="toast">' + content + '</div>'
      timer = setTimeout(() => {
        document.getElementById('toastWrap').innerHTML = ''
      }, 2000)
    }
  }
}
