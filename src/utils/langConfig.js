import ZH from '../assets/lang/zh.json'
import EN from '../assets/lang/en.json'
import ZHTW from '../assets/lang/zh-tw.json'
import AR from '../assets/lang/AR.json'
import DE from '../assets/lang/DE.json'
import ES from '../assets/lang/ES.json'
import FR from '../assets/lang/FR.json'
import ID from '../assets/lang/ID.json'
import IT from '../assets/lang/IT.json'
import JA from '../assets/lang/JA.json'
import KO from '../assets/lang/KO.json'
import NL from '../assets/lang/NL.json'
import PL from '../assets/lang/PL.json'
import PT from '../assets/lang/PT.json'
import RU from '../assets/lang/RU.json'
import SV from '../assets/lang/SV.json'
import TH from '../assets/lang/TH.json'
import TR from '../assets/lang/TR.json'
import VI from '../assets/lang/VI.json'

export default {
  config: {
    'zh-cn': ZH,
    'zh-tw': ZHTW,
    'en': EN,
    'ar': AR,
    'de': DE,
    'es': ES,
    'fr': FR,
    'id': ID,
    'it': IT,
    'ja': JA,
    'ko': KO,
    'nl': NL,
    'pl': PL,
    'pt': PT,
    'ru': RU,
    'sv': SV,
    'th': TH,
    'tr': TR,
    'vi': VI
  },
  code: [
    {
      code: 'en',
      name: 'English'
    },
    {
      code: 'zh-cn',
      name: '简体中文'
    },
    {
      code: 'zh-tw',
      name: '繁體中文'
    },
    {
      code: 'ja',
      name: '日本語'
    },
    {
      code: 'ko',
      name: '한국어'
    },
    {
      code: 'ru',
      name: 'Русский'
    },
    {
      code: 'de',
      name: 'Deutsch'
    },
    {
      code: 'fr',
      name: 'Français'
    },
    {
      code: 'it',
      name: 'Italiano'
    },
    {
      code: 'pl',
      name: 'Polski'
    },
    {
      code: 'tr',
      name: 'Türkçe'
    },
    {
      code: 'es',
      name: 'Español'
    },
    {
      code: 'pt',
      name: 'Português'
    },
    {
      code: 'sv',
      name: 'Svenska'
    },
    {
      code: 'th',
      name: 'ภาษาอังกฤษ'
    },
    {
      code: 'ar',
      name: 'العربية'
    },
    {
      code: 'id',
      name: 'Bahasa Indonesia'
    },
    {
      code: 'nl',
      name: 'Nederlands'
    },
    {
      code: 'vi',
      name: 'Tiếng Việt'
    }
  ]
}
