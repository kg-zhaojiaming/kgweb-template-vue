import { setLog } from '@/api'

export default{
  install (Vue, options) {
    Vue.prototype.setLog = function (logInfo, callback, Vue, options) {
      if (this.$route.query.preview) { return }
      let info = {
        data_version: '2.0',
        session_id: '',
        event: 'CMS',
        user_id: this.$route.query.fpid,
        app_id: this.$route.query.gameid
      }
      info['properties'] = logInfo
      info['properties']['d_c1'] = {
        value: 'Web',
        key: 'datafrom'
      }
      info['properties']['m1'] = {
        value: this.$route.query.uid,
        key: 'uid'
      }
      let params = {
        log: JSON.stringify(info)
      }
      setLog(params)
        .then((res) => {
          // ...
          // console.log(res)
          if (callback) {
            callback()
          }
        })
        .catch(function (err) {
          console.log(err)
          if (callback) {
            callback()
          }
          // this.$message.error(err)
        })
    }
  }
}
